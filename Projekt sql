-- 1. Stwórz Bazę „Sklep odzieżowy”
create database sklep_odzieżowy;

-- 2. Utwórz tabelę „Producenci” z kolumnami:
-- id producenta
-- nazwa producenta
-- adres producenta
-- nip producenta
-- data podpisania umowy z producentem
-- Do każdej kolumny ustaw odpowiedni „constraint”

create table producenci (
    id_producenta INT PRIMARY KEY,
    nazwa_producenta TEXT NOT NULL,
    adres_producenta  TEXT NOT NULL,
    nip_producenta CHAR(10) NOT NULL,
    data_podpisania_umowy DATE NOT NULL
);


-- 3. Utwórz tabelę „Produkty” z kolumnami:
-- id produktu
-- id producenta
-- nazwa produktu
-- opis produktu
-- cena netto zakupu
-- cena brutto zakupu
-- cena netto sprzedaży
-- cena brutto sprzedaży
-- procent VAT sprzedaży
-- Do każdej kolumny ustaw odpowiedni „constraint”

CREATE TABLE Produkty (
    id_produktu INT PRIMARY KEY,
    id_producenta INT,
    nazwa_produktu VARCHAR(255) NOT NULL,
    opis_produktu TEXT,
    cena_netto_zakupu DECIMAL(10,2) NOT NULL,
    cena_brutto_zakupu DECIMAL(10,2) NOT NULL,
    cena_netto_sprzedazy DECIMAL(10,2) NOT NULL,
    cena_brutto_sprzedazy DECIMAL(10,2) NOT NULL,
    procent_VAT_sprzedazy DECIMAL(5,2) NOT NULL,
    FOREIGN KEY (id_producenta) REFERENCES Producenci(id_producenta)
);

-- 4. Utwórz tabelę „Zamówienia” z kolumnami:
-- id zamówienia
-- id klienta
-- id produktu
-- Data zamówienia
-- Do każdej kolumny ustaw odpowiedni „constraint”
CREATE TABLE Zamówienia (
    id_zamówienia INT PRIMARY KEY,
    id_klienta INT,
    id_produktu INT,
    Data_zamówienia DATE NOT NULL,
    FOREIGN KEY (id_klienta) REFERENCES Klienci(id_klienta),
    FOREIGN KEY (id_produktu) REFERENCES Produkty(id_produktu)
);

-- 5. Utwórz tabelę „Klienci” z kolumnami:
-- id klienta
-- imię
-- nazwisko
-- adres
-- Do każdej kolumny ustaw odpowiedni „constraint”

CREATE TABLE Klienci (
    id_klienta INT PRIMARY KEY,
    imię Text NOT NULL,
    nazwisko Text NOT NULL,
    adres Text NOT NULL
);

-- 6. Połącz tabele ze sobą za pomocą kluczy obcych:
-- Produkty – Producenci
-- Zamówienia – Produkty
-- Zamówienia - Klienci
-- Połączenie tabeli "Produkty" z tabelą "Producenci":

ALTER TABLE Produkty
ADD CONSTRAINT fk_producent
FOREIGN KEY (id_producenta) REFERENCES Producenci(id_producenta);
-- Połączenie tabeli "Zamówienia" z tabelą "Produkty":

ALTER TABLE Zamówienia
ADD CONSTRAINT fk_produkt
FOREIGN KEY (id_produktu) REFERENCES Produkty(id_produktu);
-- Połączenie tabeli "Zamówienia" z tabelą "Klienci":

ALTER TABLE Zamówienia
ADD CONSTRAINT fk_klient
FOREIGN KEY (id_klienta) REFERENCES Klienci(id_klienta);

-- 7. Każdą tabelę uzupełnij danymi wg:
-- Tabela „Producenci” – 4 pozycje
-- Tabela „Produkty” – 20 pozycji
-- Tabela „Zamówienia” – 10 pozycji
-- Tabela „Klienci” – 10 pozycji

INSERT INTO Producenci (id_producenta, nazwa_producenta, adres_producenta, nip_producenta, data_podpisania_umowy)
VALUES 
(1, 'Producent A', 'Adres A', '1234567890', '2023-01-01'),
(2, 'Producent B', 'Adres B', '0987654321', '2023-01-15'),
(3, 'Producent C', 'Adres C', '5678901234', '2023-02-01'),
(4, 'Producent D', 'Adres D', '9876543210', '2023-02-15');

-- Produkty dla Producenta 1
INSERT INTO Produkty (id_produktu, id_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_VAT_sprzedazy)
VALUES
(1, 1, 'Koszula bawełniana', 'Opis koszuli', 40.00, 49.20, 80.00, 98.40, 23),
(2, 1, 'Spodnie dżinsowe', 'Opis spodni', 60.00, 73.80, 120.00, 147.60, 23),
(3, 1, 'T-shirt basic', 'Opis T-shirtu', 20.00, 24.60, 40.00, 49.20, 23),
(4, 1, 'Marynarka casual', 'Opis marynarki', 100.00, 123.00, 200.00, 246.00, 23),
(5, 1, 'Jeansy skinny', 'Opis jeansów', 50.00, 61.50, 100.00, 123.00, 23);

-- Produkty dla Producenta 2
INSERT INTO Produkty (id_produktu, id_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_VAT_sprzedazy)
VALUES
(6, 2, 'Koszula w kratę', 'Opis koszuli', 45.00, 55.35, 90.00, 111.60, 23),
(7, 2, 'Jeansy regular', 'Opis jeansów', 55.00, 67.65, 110.00, 135.30, 23),
(8, 2, 'Sweter z wełny', 'Opis swetra', 70.00, 86.10, 140.00, 172.20, 23),
(9, 2, 'Kurtka parka', 'Opis kurtki', 120.00, 147.60, 240.00, 295.20, 23),
(10, 2, 'Spódnica dżinsowa', 'Opis spódnicy', 40.00, 49.20, 80.00, 98.40, 23);

-- Produkty dla Producenta 3
INSERT INTO Produkty (id_produktu, id_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_VAT_sprzedazy)
VALUES
(11, 3, 'Sukienka wieczorowa', 'Opis sukienki', 90.00, 110.70, 180.00, 221.40, 23),
(12, 3, 'Płaszcz wełniany', 'Opis płaszcza', 150.00, 184.50, 300.00, 369.00, 23),
(13, 3, 'Bluzka z koronką', 'Opis bluzki', 35.00, 43.05, 70.00, 86.10, 23),
(14, 3, 'Spódnica plisowana', 'Opis spódnicy', 55.00, 67.65, 110.00, 135.30, 23),
(15, 3, 'Sweter oversize', 'Opis swetra', 60.00, 73.80, 120.00, 147.60, 23);

-- Produkty dla Producenta 4
INSERT INTO Produkty (id_produktu, id_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_VAT_sprzedazy)
VALUES
(16, 4, 'Garnitur slim fit', 'Opis garnituru', 200.00, 246.00, 400.00, 492.00, 23),
(17, 4, 'Koszula w paski', 'Opis koszuli', 45.00, 55.35, 90.00, 111.60, 23),
(18, 4, 'Jeansy bootcut', 'Opis jeansów', 60.00, 73.80, 120.00, 147.60, 23),
(19, 4, 'T-shirt polo', 'Opis T-shirtu', 25.00, 30.75, 50.00, 61.50, 23),
(20, 4, 'Kurtka bomber', 'Opis kurtki', 80.00, 98.40, 160.00, 196.80, 23);

 INSERT INTO Klienci (id_klienta, imię, nazwisko, adres)
VALUES
(1, 'Jan', 'Kowalski', 'ul. Kwiatowa 5, 00-001 Warszawa'),
(2, 'Anna', 'Nowak', 'ul. Leśna 10, 50-002 Wrocław'),
(3, 'Piotr', 'Wiśniewski', 'ul. Słoneczna 3, 30-003 Kraków'),
(4, 'Katarzyna', 'Dąbrowska', 'ul. Polna 15, 20-004 Poznań'),
(5, 'Michał', 'Lewandowski', 'ul. Zielona 20, 10-005 Gdańsk'),
(6, 'Alicja', 'Wójcik', 'ul. Ogrodowa 25, 40-006 Katowice'),
(7, 'Tomasz', 'Kamiński', 'ul. Miodowa 30, 60-007 Łódź'),
(8, 'Magdalena', 'Zielińska', 'ul. Kwiatowa 35, 70-008 Lublin'),
(9, 'Kamil', 'Szymański', 'ul. Wiejska 40, 80-009 Bydgoszcz'),
(10, 'Monika', 'Woźniak', 'ul. Szkolna 45, 90-010 Szczecin');

INSERT INTO Zamówienia (id_zamówienia, id_klienta, id_produktu, Data_zamówienia)
VALUES
(1, 1, 5, '2024-01-05'),
(2, 2, 8, '2024-01-10'),
(3, 3, 12, '2024-01-15'),
(4, 4, 17, '2024-01-20'),
(5, 5, 3, '2024-01-25'),
(6, 6, 9, '2024-01-30'),
(7, 7, 14, '2024-02-05'),
(8, 8, 19, '2024-02-10'),
(9, 9, 6, '2024-02-15'),
(10, 10, 1, '2024-02-20');

-- 8. Wyświetl wszystkie produkty z wszystkimi danymi od producenta który znajduje się na pozycji 1 w tabeli „Producenci”
SELECT p.*
FROM Produkty p
JOIN Producenci pr ON p.id_producenta = pr.id_producenta
WHERE pr.id_producenta = 1;

-- 9. Posortuj te produkty alfabetycznie po nazwie
SELECT p.*
FROM Produkty p
JOIN Producenci pr ON p.id_producenta = pr.id_producenta
WHERE pr.id_producenta = 1
ORDER BY p.nazwa_produktu;

-- 10. Wylicz średnią cenę za produktu od producenta z pozycji 1
SELECT AVG(cena_brutto_sprzedazy) AS średnia_cena
FROM Produkty p
JOIN Producenci pr ON p.id_producenta = pr.id_producenta
WHERE pr.id_producenta = 1;

-- 11. Wyświetl dwie grupy produktów tego producenta:
-- Połowa najtańszych to grupa: „Tanie”
-- Pozostałe to grupa: „Drogie”

WITH RankedProducts AS (
SELECT 
nazwa_produktu,
cena_brutto_sprzedazy,
ROW_NUMBER() OVER (ORDER BY cena_brutto_sprzedazy) AS rn,
COUNT(*) OVER () AS total_count
FROM Produkty
WHERE id_producenta = 1
)
SELECT 
nazwa_produktu,
cena_brutto_sprzedazy,
CASE 
WHEN rn <= total_count / 2 THEN 'Tanie'
ELSE 'Drogie'
END AS grupa_cenowa
FROM RankedProducts;

-- 12. Wyświetl produkty zamówione, wyświetlając tylko ich nazwę
SELECT p.nazwa_produktu
FROM Zamówienia z
JOIN Produkty p ON z.id_produktu = p.id_produktu;

-- 13. Wyświetl wszystkie produkty zamówione – ograniczając wyświetlanie do 5 pozycji
 SELECT p.*
FROM Zamówienia z
JOIN Produkty p ON z.id_produktu = p.id_produktu
LIMIT 5;

-- 14. Policz łączną wartość wszystkich zamówień
SELECT SUM(cena_brutto_sprzedazy) AS łączna_wartość_zamówień
FROM Zamówienia z
JOIN Produkty p ON z.id_produktu = p.id_produktu;

-- 15. Wyświetl wszystkie zamówienia wraz z nazwą produktu sortując je wg daty od najstarszego do najnowszego
SELECT z.id_zamówienia, z.Data_zamówienia, p.nazwa_produktu
FROM Zamówienia z
JOIN Produkty p ON z.id_produktu = p.id_produktu
ORDER BY z.Data_zamówienia;

-- 16. Sprawdź czy w tabeli produkty masz uzupełnione wszystkie dane – wyświetl pozycje dla których brakuje danych
SELECT *
FROM Produkty
WHERE id_producenta IS NULL
   OR nazwa_produktu IS NULL
   OR opis_produktu IS NULL
   OR cena_netto_zakupu IS NULL
   OR cena_brutto_zakupu IS NULL
   OR cena_netto_sprzedazy IS NULL
   OR cena_brutto_sprzedazy IS NULL
   OR procent_VAT_sprzedazy IS NULL;

-- 17. Wyświetl produkt najczęściej sprzedawany wraz z jego ceną
SELECT p.nazwa_produktu, p.cena_brutto_sprzedazy
FROM Zamówienia z
JOIN Produkty p ON z.id_produktu = p.id_produktu
GROUP BY p.nazwa_produktu, p.cena_brutto_sprzedazy
ORDER BY COUNT(*) DESC
LIMIT 1;

-- 18. Znajdź dzień w którym najwięcej zostało złożonych zamówień
SELECT Data_zamówienia, COUNT(*) AS liczba_zamówień
FROM Zamówienia
GROUP BY Data_zamówienia
ORDER BY COUNT(*) DESC
LIMIT 1;
